package com.bora.easydictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class Dictionary extends Fragment{
	public static EditText word2;
	private String currentWord="";
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode==10)
			Toast.makeText(getActivity(),"Thank you very much for the feedback.", Toast.LENGTH_LONG).show();
	}

	private String currentMeaning="";
	public TextView result2;
	public ProgressDialog dialog;
	public String word=null;
	public String meaning=null;
	AlertDialog alert=null;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dictionary, container, false);
		setHasOptionsMenu(true);
		word2 = (EditText) view.findViewById(R.id.editText1);
		result2 = (TextView) view.findViewById(R.id.textView1);
		result2.setMovementMethod(new ScrollingMovementMethod());
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(word2.getWindowToken(), 0);
		word2.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction()!=KeyEvent.ACTION_DOWN)
					return false;
				if(keyCode == KeyEvent.KEYCODE_ENTER){
					if(word2.getText().toString().isEmpty()){
						InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(word2.getWindowToken(), 0);
						Toast.makeText(getActivity(),R.string.empty_word, Toast.LENGTH_LONG).show();
					}
					else
						search();
				}
				return false;
			}
		});
		return view;       
	}

	public void search() {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(word2.getWindowToken(), 0);
		if(MainActivity.words.indexOf(word2.getText().toString())!=-1){
			result2.setText(MainActivity.meanings.get(MainActivity.words.indexOf(word2.getText().toString())));
		}
		else if(isNetworkAvailable()){
			String word=word2.getText().toString();
			try {
				FindMeaning findMeaning = new FindMeaning();
				dialog = new ProgressDialog(getActivity());
				dialog.setCancelable(true);
				dialog.setMessage(getString(R.string.searching));
				findMeaning.execute(word);
				dialog.show();
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		else{
			result2.setText(R.string.check_internet);
			Toast.makeText(getActivity(),R.string.check_internet, Toast.LENGTH_SHORT).show();
		}
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem menu) {
		switch (menu.getItemId()) {
		case R.id.share_with_friends:
			if(currentWord.isEmpty())
				Toast.makeText(getActivity(),"Please search some word first.", Toast.LENGTH_LONG).show();
			else{
				String smsText="Word : "+currentWord+"\nMeaning : \n"+currentMeaning+"\n\n\n"+getResources().getString(R.string.sent_via);
				sendSMS(smsText);
			}
			return true;
		case R.id.about:
			showAbout();
			return true;
		case R.id.help1:
			showHelp1();
			return true;
		default:
			return super.onOptionsItemSelected(menu);
		}
	}

	private void showHelp1() {
		// TODO Auto-generated method stub
		View messageView = getActivity().getLayoutInflater().inflate(R.layout.about, null, false);
		TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
		textView.setText(R.string.help_content_1);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.app_name);
		builder.setView(messageView);
		builder.create();
		builder.show();
	}

	private void showAbout() {
		// TODO Auto-generated method stub
		final View messageView = getActivity().getLayoutInflater().inflate(R.layout.about, null, false);

		TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
		TextView t2 = (TextView) messageView.findViewById(R.id.email_field);
		t2.setText(Html.fromHtml("<a href>easydictionary@outlook.com</a>"));
		t2.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub   
				Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
				sendIntent.setData(Uri.parse("mailto:" + "easydictionary@outlook.com")); 
				sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_subject));
				sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "easydictionary@outlook.com" });
				startActivityForResult(sendIntent, 10);
				if(alert!=null)
					alert.cancel();
			}
		});
		textView.setMovementMethod(LinkMovementMethod.getInstance());
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.app_name);
		builder.setView(messageView);
		alert = builder.create();
		alert.show();
	}
	public void sendSMS(String smsText) 
	{ 
		Intent waIntent = new Intent(Intent.ACTION_SEND);
		waIntent.setType("text/plain");
		String text = smsText;
		waIntent.putExtra(Intent.EXTRA_TEXT, text);//
		startActivity(Intent.createChooser(waIntent, "Share with"));
	} 

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		getActivity().getMenuInflater().inflate(R.menu.menu1, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	private class FindMeaning extends AsyncTask<String, Void, String>{
		String meaning=null;
		String word=null;
		@Override
		protected String doInBackground(String... params) {
			getMethod test = new getMethod(getActivity());
			word=word2.getText().toString();
			try {
				meaning = test.getInternetData(word);
			} catch (Exception e) {
				e.printStackTrace();
			}	
			return meaning;
		}

		@Override
		protected void onPostExecute(String meaning) {
			// TODO Auto-generated method stub
			super.onPostExecute(meaning);
			result2.setText(meaning);
			if (dialog != null) {
				dialog.dismiss();
				dialog=null;
			}
			if(!meaning.equals(getResources().getString(R.string.sorryNoWordFound)) && !word.isEmpty() && !meaning.equals(getResources().getString(R.string.search_first))){
				currentMeaning=meaning;
				currentWord=word;
				MainActivity.words.add(word);
				MainActivity.meanings.add(meaning); 
				MainActivity.levels.add(0);
				MainActivity.totalWords+=1;
				WriteToDB writeToDB = new WriteToDB();
				writeToDB.execute(word, meaning);
			}
			else{
				Toast.makeText(getActivity(),R.string.sorryNoWordFound, Toast.LENGTH_SHORT).show();
			}
		}    	
	}
	private class WriteToDB extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... params) {
			MyDBClass mDbHelper = new MyDBClass(getActivity());
			SQLiteDatabase db = mDbHelper.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(MyDBClass.COLUMN_WORD, params[0]);
			values.put(MyDBClass.COLUMN_MEANING, params[1]);
			values.put(MyDBClass.COLUMN_LEVEL, 0);
			db.insert(MyDBClass.TABLE_NAME, null, values);
			db.close();
			return null;
		}    	
	}
	@Override
	public void onPause() {
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(word2.getWindowToken(), 0);
		super.onPause();
	}

	@Override
	public void onResume() {
		result2.setText(currentMeaning);
		super.onResume();
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}

