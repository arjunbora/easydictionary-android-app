package com.bora.easydictionary;

import java.util.List;
import java.util.Stack;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Remember extends Fragment {
	public TextView wordBox;
	public TextView meaningBox;
	public Button b1;
	private int currentIndex=0;
	SharedPreferences sharedpreferences;
	public static final String MyPREFERENCES = "MyPrefs" ;
	String showInstructions="yes";
	public static Animation animationFalling=null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view;
		if(getActivity().getResources().getConfiguration().orientation ==  2){
			view = inflater.inflate(R.layout.remember_landscape, container, false);
		}
		else{
			view = inflater.inflate(R.layout.remember, container, false);
		}

		setHasOptionsMenu(true);
		sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		View checkBoxView = View.inflate(getActivity(), R.layout.checkbox, null);
		CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
		wordBox = (TextView) view.findViewById(R.id.wordBox);
		meaningBox = (TextView) view.findViewById(R.id.meaningBox);
		meaningBox.setMovementMethod(new ScrollingMovementMethod());
		b1 = (Button) view.findViewById(R.id.button1);
		ScrollView rL=(ScrollView) view.findViewById(R.id.scrollView2);
		animationFalling = AnimationUtils.loadAnimation(getActivity(), R.anim.swiperight);

		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Editor editor = sharedpreferences.edit();
				editor.putString("showInstructions", "no");
				editor.commit();
			}
		});
		checkBox.setText(R.string.dont_show_again);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose preference");
		if (sharedpreferences.contains("showInstructions"))
			showInstructions=sharedpreferences.getString("showInstructions","");
		if(showInstructions.equals("yes")){
			builder.setMessage(R.string.instructions)
			.setView(checkBoxView)
			.setCancelable(false)
			.setNeutralButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			})
			.show();
		}

		if(MainActivity.totalWords!=0){
			wordBox.setText(MainActivity.words.get(currentIndex%MainActivity.totalWords));
			meaningBox.setText("");
		}
		else{
			wordBox.setText("");
			meaningBox.setText(R.string.search_first);
		}

		ImageButton correct = (ImageButton) view.findViewById(R.id.right);
		correct.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				MainActivity.writableDB.execSQL("UPDATE words SET level = level + 1 WHERE word='"+ wordBox.getText().toString()  +"'");	
				swipeLeft();
			}
		});
		ImageButton incorrect = (ImageButton) view.findViewById(R.id.wrong);
		incorrect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainActivity.writableDB.execSQL("UPDATE words SET level = level - 1 WHERE word='"+ wordBox.getText().toString()  +"'");
				swipeRight();
			}
		});

		ImageButton delete = (ImageButton) view.findViewById(R.id.delete);
		delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(wordBox.getText().toString().isEmpty())
					Toast.makeText(getActivity(),R.string.search_first, Toast.LENGTH_LONG).show();
				else {
					MainActivity.writableDB.delete(MyDBClass.TABLE_NAME, MyDBClass.COLUMN_WORD + "='" + wordBox.getText().toString()+"'", null);
					int index=MainActivity.words.indexOf(wordBox.getText().toString());
					MainActivity.words.remove(index);
					MainActivity.meanings.remove(index);
					MainActivity.levels.remove(index);
					MainActivity.totalWords-=1;
					currentIndex-=1;
					if(currentIndex<0)
						currentIndex+=MainActivity.totalWords;
					swipeLeft();
				}
			}
		});

		ImageButton share = (ImageButton) view.findViewById(R.id.share);
		share.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View v) {
				if(wordBox.getText().toString().isEmpty())
					Toast.makeText(getActivity(),R.string.search_first, Toast.LENGTH_LONG).show();
				else{
					String smsText="Word : "+wordBox.getText().toString()+"\nMeaning :\n"+MainActivity.meanings.get(currentIndex%MainActivity.totalWords)+"\n\n\n"+getResources().getString(R.string.sent_via);
					sendSMS(smsText);
				}			
			}


			public void sendSMS(String smsText) 
			{ 
				Intent waIntent = new Intent(Intent.ACTION_SEND);
				waIntent.setType("text/plain");
				waIntent.putExtra(Intent.EXTRA_TEXT, smsText);
				waIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.feedback_subject));
				startActivity(createEmailOnlyChooserIntent(waIntent, "Share with", smsText));
			}


			private Intent createEmailOnlyChooserIntent(Intent i, CharSequence chooserTitle, String text) {
				// TODO Auto-generated method stub
				Stack<Intent> intents = new Stack<Intent>();
				i = new Intent(Intent.ACTION_SEND);
				i.setType("text/plain");
				i.putExtra(Intent.EXTRA_TEXT, text);
				List<ResolveInfo> activities = getActivity().getPackageManager().queryIntentActivities(i, 0);
				Log.d("EZD",String.valueOf(activities.size()));
				for(ResolveInfo ri : activities) {
					Intent target = new Intent(i);
					String packageName = ri.activityInfo.packageName;
					if(!packageName.toLowerCase().contains("docs") && !packageName.toLowerCase().contains("bluetooth") && !packageName.toLowerCase().contains("fileshareclient")) {
						target.setPackage(packageName);
						Log.d("EZD",packageName.toLowerCase());
						intents.add(target);
			        }					
				}
				if(!intents.isEmpty()) {
					Intent chooserIntent = Intent.createChooser(intents.remove(0), chooserTitle);
					chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Parcelable[intents.size()]));
					return chooserIntent;
				} else {
					return Intent.createChooser(i, chooserTitle);
				}
			} 
		});

		final GestureDetector gesture = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onSingleTapConfirmed(MotionEvent e) {
				if(!wordBox.getText().toString().isEmpty())
					meaningBox.setText(MainActivity.meanings.get(currentIndex%MainActivity.totalWords));
				return super.onSingleTapConfirmed(e);
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
				final int SWIPE_MIN_DISTANCE = 120;
				final int SWIPE_MAX_OFF_PATH = 250;
				final int SWIPE_THRESHOLD_VELOCITY = 200;
				try {
					if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
						return false;
					if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY){
						swipeLeft();			
					}

					else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY){
						swipeRight();			
					}
				} catch (Exception e) {

				}
				return super.onFling(e1, e2, velocityX, velocityY);
			}
		});

		meaningBox.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return gesture.onTouchEvent(event);
			}
		});

		rL.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return gesture.onTouchEvent(event);
			}

		});

		view.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return gesture.onTouchEvent(event);
			}
		});	

		animationFalling.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				setPreviousWord();
			}
		});
		return view;	
	} 

	private void swipeRight() {
		currentIndex-=1;
		if(currentIndex<0)
			currentIndex+=MainActivity.totalWords;
		wordBox.startAnimation(animationFalling);
		if(!meaningBox.getText().toString().equals(getString(R.string.search_first))){
			meaningBox.startAnimation(animationFalling);
		}
	}
	private void swipeLeft() {
		currentIndex+=1;
		wordBox.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.swipeleft));
		setNextWord();
	}
	private void setNextWord() {
		if(MainActivity.totalWords!=0){
			wordBox.setText(MainActivity.words.get(currentIndex%MainActivity.totalWords));
			meaningBox.setText("");
		}
		else{
			meaningBox.setText(R.string.search_first);
			wordBox.setText("");
		}
	}
	private void setPreviousWord() {
		if(MainActivity.totalWords!=0){
			wordBox.setText(MainActivity.words.get(currentIndex%MainActivity.totalWords));
			meaningBox.setText("");
		}
		else
			meaningBox.setText(R.string.search_first);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menu) {
		switch (menu.getItemId()) {
		case R.id.help2:
			showHelp1();
			return true;
		default:
			return super.onOptionsItemSelected(menu);
		}
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		getActivity().getMenuInflater().inflate(R.menu.main, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	private void showHelp1() {
		// TODO Auto-generated method stub
		View messageView = getActivity().getLayoutInflater().inflate(R.layout.about, null, false);
		TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
		textView.setTextSize(15);
		
		Drawable d1 = getResources().getDrawable(R.drawable.right); 
		d1.setBounds(0, 0, d1.getIntrinsicWidth()/2, d1.getIntrinsicHeight()/2);
		SpannableString s1 = new SpannableString("    "+getResources().getString(R.string.right_help)); 
		ImageSpan span1 = new ImageSpan(d1, ImageSpan.ALIGN_BASELINE); 
		s1.setSpan(span1, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 

		Drawable d2 = getResources().getDrawable(R.drawable.wrong); 
		d2.setBounds(0, 0, d2.getIntrinsicWidth()/2, d2.getIntrinsicHeight()/2);
		SpannableString s2 = new SpannableString("    "+getResources().getString(R.string.wrong_help)); 
		ImageSpan span2 = new ImageSpan(d2, ImageSpan.ALIGN_BASELINE); 
		s2.setSpan(span2, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 

		Drawable d3 = getResources().getDrawable(R.drawable.share); 
		d3.setBounds(0, 0, d3.getIntrinsicWidth()/2, d3.getIntrinsicHeight()/2);
		SpannableString s3 = new SpannableString("    "+getResources().getString(R.string.share_help)); 
		ImageSpan span3 = new ImageSpan(d3, ImageSpan.ALIGN_BASELINE); 
		s3.setSpan(span3, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 

		Drawable d4 = getResources().getDrawable(R.drawable.delete); 
		d4.setBounds(0, 0, d4.getIntrinsicWidth()/2, d4.getIntrinsicHeight()/2);
		SpannableString s4 = new SpannableString("    "+getResources().getString(R.string.delete_help)); 
		ImageSpan span4 = new ImageSpan(d4, ImageSpan.ALIGN_BASELINE); 
		s4.setSpan(span4, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 

		textView.setText(TextUtils.concat(s1,"\n",s2,"\n",s3,"\n",s4));
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.app_name);
		builder.setView(messageView);
		builder.create();
		builder.show();
	}
}