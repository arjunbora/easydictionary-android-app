package com.bora.easydictionary;

import java.io.StringReader;
import java.net.URI;
import java.util.Hashtable;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

public class getMethod {
	String meaning="";
	boolean isFirstType=true;
	Context c;
	Hashtable<String, String> type = new Hashtable<String, String>();
	public getMethod(Context c) {
		this.c=c;
		type.put("v", "Verb :");
		type.put("adj", "Adjective :");
		type.put("n", "Noun :");
		type.put("adv", "Adverb :");
	}
	@SuppressLint("DefaultLocale")
	public String getInternetData(String word) throws Exception{


		HttpClient client = new DefaultHttpClient();
		word=word.replace(" ", "%20");
		URI website = new URI("http://services.aonaware.com/DictService/DictService.asmx/DefineInDict?dictId=wn&word="+word);
		//URI website = new URI("http://services.aonaware.com/DictService/DictService.asmx/Define?word="+word);
		//URI website = new URI("http://www.dictionaryapi.com/api/v1/references/learners/xml/exaggerate?key=89ec701e-cb48-409e-bf99-858d80188964");
		HttpGet request = new HttpGet();
		request.setURI(website);
		HttpResponse response = client.execute(request);
		HttpEntity r_entity = response.getEntity();
		String xmlString = EntityUtils.toString(r_entity);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = factory.newDocumentBuilder();
		InputSource inStream = new InputSource();
		inStream.setCharacterStream(new StringReader(xmlString));
		Document doc = db.parse(inStream);  
		NodeList nl1 = doc.getElementsByTagName("WordDefinition");
		meaning="";
		for(int i = 1; i < nl1.getLength(); i++) {
			if (nl1.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				org.w3c.dom.Element nameElement = (org.w3c.dom.Element) nl1.item(i);
				meaning = nameElement.getFirstChild().getNodeValue().trim();
				//Log.d("EZD","value changed");
			}
		}
		String pattern = "(\\s+)";
		meaning=meaning.replaceAll(pattern, " ");
		word=word.replace("%20", " ");
		pattern = "(("+word+" )|("+word.toUpperCase(Locale.US)+" ))";
		meaning=meaning.replaceFirst(pattern, ""); 		//to change only the first occurrence of the word, word may appear in examples
		pattern = "(\\d+)";
		meaning=meaning.replaceAll(pattern, "\n$1");
		Pattern pattern2 = Pattern.compile("((adv)|(adj)|([n|v]))(( \n)|( :))");
		Matcher m = pattern2.matcher(meaning);
		StringBuffer sb = new StringBuffer();
		while (m.find())
			m.appendReplacement(sb, getType((m.group()).replace(":", "").trim())+"\n");
		m.appendTail(sb);
		meaning=sb.toString();
		if(meaning.isEmpty())
			return c.getString(R.string.sorryNoWordFound);
		else
			return meaning;
	}
	private String getType(String string) {
		String tmp=null;
		if(isFirstType){
			tmp=type.get(string).toString();
			isFirstType=false;
		}
		else
			tmp="\n\n"+type.get(string).toString();
		return tmp;
	}
}