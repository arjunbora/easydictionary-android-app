package com.bora.easydictionary;

import java.util.ArrayList;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
public class MainActivity extends Activity {
	private static final String TAB_KEY_INDEX = "tab_key";
	public static ArrayList<String> words = new ArrayList<String>();
	public static ArrayList<String> meanings = new ArrayList<String>();
	public static ArrayList<Integer> levels = new ArrayList<Integer>();
	public static int totalWords=0;
	public static MyDBClass mDbHelper;
	public static SQLiteDatabase writableDB;
	public static SQLiteDatabase readableDB;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_main);
		
		ActionBar actionbar = getActionBar();
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		ActionBar.Tab dictionaryTab = actionbar.newTab().setText(R.string.dictionary_tab);
		ActionBar.Tab rememberTab = actionbar.newTab().setText(R.string.keep_in_mind_tab);

		Fragment mDictFragmant = new Dictionary();
		Fragment mRemeberFragment = new Remember();

		dictionaryTab.setTabListener(new MyTabsListener(mDictFragmant, getApplicationContext()));
		rememberTab.setTabListener(new MyTabsListener(mRemeberFragment,	getApplicationContext()));

		actionbar.addTab(dictionaryTab);
		actionbar.addTab(rememberTab);

		if (savedInstanceState != null) {
			actionbar.setSelectedNavigationItem(savedInstanceState.getInt(TAB_KEY_INDEX, 0));
		}

		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		writableDB.close();
		readableDB.close();
		mDbHelper.close();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDbHelper = new MyDBClass(this);
		writableDB = mDbHelper.getWritableDatabase();
		readableDB = mDbHelper.getReadableDatabase();	
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		MyDBClass mDbHelper2 = new MyDBClass(this);
		String[] releventColumns = {MyDBClass.COLUMN_WORD, MyDBClass.COLUMN_MEANING, MyDBClass.COLUMN_LEVEL };
		SQLiteDatabase db2 = mDbHelper2.getReadableDatabase();
		Cursor c = db2.query(
				MyDBClass.TABLE_NAME,  // The table to query
				releventColumns,                               // The columns to return
				null,                                // The columns for the WHERE clause
				null,                            // The values for the WHERE clause
				null,                                     // don't group the rows
				null,                                     // don't filter by row groups
				MyDBClass.COLUMN_LEVEL+" DESC"                                 // The sort order
				);
		c.moveToLast();
		int i=0;
		for(i=0; i<c.getCount(); i++){
			words.add(c.getString(c.getColumnIndexOrThrow(MyDBClass.COLUMN_WORD)));
			meanings.add(c.getString(c.getColumnIndexOrThrow(MyDBClass.COLUMN_MEANING)));
			levels.add(c.getInt(c.getColumnIndexOrThrow(MyDBClass.COLUMN_LEVEL)));
			//Log.d("EZD", words.get(i)+" "+meanings.get(i)+" "+levels.get(i));
			c.moveToPrevious();
		}
		c.close();
		db2.close();
		mDbHelper2.close();
		totalWords=i;
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Log.d("EZD","words will be cleared");
		words.clear();
		meanings.clear();
		levels.clear();
		totalWords=0;
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	

}

class MyTabsListener implements ActionBar.TabListener {
	public Fragment fragment;
	public Context context;
	public MyTabsListener(Fragment fragment, Context context) {
		this.fragment = fragment;
		this.context = context;
	}
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		ft.replace(R.id.fragment_container, fragment);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			
			InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(Dictionary.word2.getWindowToken(), 0);
		ft.remove(fragment);
	}
}

