package com.bora.easydictionary;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBClass extends SQLiteOpenHelper {
	public static final String TABLE_NAME = "words";
	public static final String DATABASE_NAME = "dictionary.db";
	public static final int DATABASE_VERSION = 1;
	public static final String COLUMN_WORD = "word";
	public static final String COLUMN_MEANING = "meaning";
	public static final String COLUMN_LEVEL = "level";

	public static final String DICTIONARY_TABLE_CREATE = 
			"CREATE TABLE " + TABLE_NAME + " (" + COLUMN_WORD + " TEXT primary key,  " + COLUMN_MEANING + " TEXT, " + COLUMN_LEVEL +" integer);";
	
	public MyDBClass(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DICTIONARY_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
}
