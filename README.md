# README #

Goto https://play.google.com/store/apps/details?id=com.bora.easydictionary and install the app.
Alternatively you can install it directly from .apk stored in https://bitbucket.org/arjunbora/easydictionary-android-app/src/958f4ee820d09ba38b349b9609109edffd806ca1/bin/?at=master

About app:
Ever searched some word in dictionary and then forget it…and then searched it again…only to forget it again ??
Here is the solution. This app will help you retain words you search once. You can review the words you searched once and with its unique algorithm you will be shown those words more frequently which are difficult to remember for you.

This App is free, and will always be. We do not sell ads.

Features:
* Search phrasal verbs (like "play along", please see the snapshot)
* Synonyms & Antonyms
* Example Sentences
* Extremely light-weight
* Lets you review the words on the basis of how well you retain them in your memory
* Share words with your friends to let both of you remember the word in a better way

Please send your suggestion/bugs to arjun4084346@gmail.com